/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.maven.shade

import org.apache.maven.plugins.shade.relocation.Relocator
import org.apache.maven.plugins.shade.relocation.SimpleRelocator
import spock.lang.Specification
import spock.lang.Unroll

import java.util.jar.JarInputStream
import java.util.jar.JarOutputStream
import java.util.zip.ZipEntry


class PropertiesRelocatorTransformerTest extends Specification{


    static String originalTxt = """
# I'm a comment followed by a blank line

replace.me = value.for.replaced.me
	replace.me.with.trailing.spaces=value.for.replace.me.with.trailing.spaces   

# the following should not be replaced
dont.replace.me = not.replaced
not.me= not.replaced.either

 
"""

    static String relocatedTxt = """
# I'm a comment followed by a blank line

replace.me = successfully.changed.replaced.me
	replace.me.with.trailing.spaces=successfully.changed.replace.me.with.trailing.spaces   

# the following should not be replaced
dont.replace.me = not.replaced
not.me= not.replaced.either

 
"""



    // ************************ tests ************************

    @Unroll("should #humanres accept to process the resource")
    def "should accept (or not) to process the resource"(){

        expect :
        transformer.canTransformResource(resource) == res

        where :

        res    |   humanres   | resource                      |  transformer
        true    |   ""          | "shaded/bob.properties"     | transfo(true, [])
        false   |   "not"       | "shaded/bob.properties"     | transfo(false, [])
        true    |   ""          | "shaded/notpropsfile"       | transfo(true, ["shaded/notpropsfile"])
        false   |   "not"       | "shaded/notpropsfile"       | transfo(true, ["shaded/someotherfile"])
    }


    def "should prepare a new content for a shaded file"(){

        given :

            def transformer = new PropertiesRelocatorTransformer()
            def resource = "shaded/replaceme.properties"
            def is = new ByteArrayInputStream(originalTxt.getBytes())

        Relocator relocator = new SimpleRelocator("value.for", "successfully.changed", [], [])

        when :
            transformer.processResource(resource, is, [relocator])

        then :
            transformer.contentByPath[resource] == relocatedTxt

    }

    def "should insert the modified content into the jar in making"(){

        given :

            def transformer = new PropertiesRelocatorTransformer()
            def resourceName = "shaded/replaceme.properties"
            def resourceContent = relocatedTxt

            transformer.contentByPath[resourceName] = resourceContent

        and :
            ByteArrayOutputStream os = new ByteArrayOutputStream()
            JarOutputStream outjar = new JarOutputStream(os)

        when :
            transformer.modifyOutputStream outjar

        then :
            ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray())
            JarInputStream injar = new JarInputStream(is)

            ZipEntry entry = injar.getNextEntry()
            String entryContent = injar.text

            entry.name == resourceName
            entryContent == resourceContent
    }



    // ********** utils *************

    private PropertiesRelocatorTransformer transfo(allfiles, includes){
        return new PropertiesRelocatorTransformer(allfiles, includes)
    }

}
