/**
 *     This file is part of the Squashtest platform.
 *     Copyright (C) Henix, henix.fr
 *
 *     See the NOTICE file distributed with this work for additional
 *     information regarding copyright ownership.
 *
 *     This is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     this software is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with this software.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.squashtest.tm.maven.shade;

import org.apache.maven.plugins.shade.relocation.Relocator;
import org.apache.maven.plugins.shade.resource.ResourceTransformer;
import org.codehaus.plexus.util.IOUtil;

import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Applies relocation to the values of properties files.
 *
 * <h5>parameters</h5>
 *
 * <ul>
 *     <li>processEveryFiles (boolean) : default true, any file encountered that ends with .properties will be processed.</li>
 *     <li>includes : a list of &ltinclude&gt, one entry per file. The includes will be processed regardless of the value of 'processEveryFile'.
 *     The path must account for relocations and be the exact path relative to the root of its original jar.
 *     For now no file pattern nor expression is supported, and there is no 'exclusions' clause : this basically works like a white list.</li>
 * </ul>
 *
 * <h5>effects</h5>
 *
 * <p>
 *     Looks for properties definition matching the format &ltkey&gt = &ltvalue&gt. Spaces around the '=' or at the begining/end of the line will be ignored.
 *     For now values that consists of a list of classes will not be processed (eg &ltkey&gt = &ltvalue1, value2...&gt are invalid).
 *     This transformers attempts not to alter the original file beyond the relocations, comments and indentation should be retrieved as-is in output file.
 * </p>
 *
 *
 * <p>Warning : Does not merge files in case of duplicate files.</p>
 *
 * <h5>credits to</h5>
 *
 * The original implementation of org.apache.maven.plugins.shade.resource.ServicesResourceTransformer
 *
 * @author bsiri
 */
public class PropertiesRelocatorTransformer implements ResourceTransformer{


	private static final String PROPERTIES_SUFFIX = ".properties";

	private static final Pattern PATTERN = Pattern.compile("^\\s*[^=]+\\s*=\\s*(.*)\\s*$");

	/*
		Default : true -> any file ending with .properties will be considered.
		If false, the file must appears in the '<includes>' or won't be processed.
	*/
	private boolean processEveryFiles = true;

	/*
		A list of files that must be processed (.properties or not).
		Each entry must be the path of the file relative to the root of its own original jar.
		Note that no expressions nor wildcard will be processed.
	 */
	private List<String> includes = new ArrayList<String>();

	private Map<String, String> contentByPath = new HashMap<String, String>();


	public PropertiesRelocatorTransformer() {
	}

	public PropertiesRelocatorTransformer(boolean processEveryFiles, List<String> includes) {
		this.processEveryFiles = processEveryFiles;
		this.includes = includes;
	}

	public boolean canTransformResource(String resource) {
		boolean canTransform = false;
		if (processEveryFiles && resource.endsWith(PROPERTIES_SUFFIX)){
			canTransform = true;
		}
		else if (includes.contains(resource)){
			canTransform = true;
		}
		return canTransform;
	}

	public void processResource(String resource, InputStream is, List<Relocator> relocators) throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));

		StringBuilder resBuilder = new StringBuilder();

		String line;
		while((line = reader.readLine()) != null){

			String finalLine = line;

			Matcher matcher = PATTERN.matcher(finalLine);

			// here we got a property definition (ie not an empty line or a comment)
			if (matcher.matches()){

				String candidate = matcher.group(1);

				for (Relocator rel : relocators){
					if (rel.canRelocateClass(candidate)){
						String replacement = rel.applyToSourceContent(candidate);
						finalLine = finalLine.replace(candidate, replacement);
						break;
					}
				}

			}

			resBuilder.append(finalLine + "\n");

		}

		contentByPath.put(resource, resBuilder.toString());

	}

	public boolean hasTransformedResource() {
		return contentByPath.size() > 0;
	}

	public void modifyOutputStream(JarOutputStream os) throws IOException {

		for (Map.Entry<String, String> entry : contentByPath.entrySet()){

			String path = entry.getKey();
			String content = entry.getValue();

			os.putNextEntry(new JarEntry(path));
			IOUtil.copy(content, os);
			os.closeEntry();
		}

	}


}
