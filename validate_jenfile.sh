#!/bin/sh

# Sends the jenkinsfile to your local jenkins instance for syntax validation
#
# note : for windows-based machines a conversion of eol may be useful before sending the file (hence the dos2unix).
# For the same reason this very script might need eol conversion before execution.

dos2unix -n jenkinsfile jenfile.tmp
JENKINS_CRUMB=`curl 'http://localhost:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)'`
curl -X POST -H $JENKINS_CRUMB -F "jenkinsfile=<jenfile.tmp" http://localhost:8080/pipeline-model-converter/validate
rm -f jenfile.tmp
