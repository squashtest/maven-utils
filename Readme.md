# Maven utils

This project puts together various utilities used for building artifacts
that aren't available elsewhere.



## Maven shade plugin (3.0.0+) extensions

List of extensions for maven-shade-plugin. They currently target
the version 3.0.0. In case you suspect this documentation being outdated
you can check the property
*<maven-shade-plugin.version/>* in the pom.xml.

#### PropertiesRelocatorTransformer

```org.squashtest.tm.maven.shade.PropertiesRelocatorTransformer```

This resource transformer applies relocations to the properties files in
the flattened embedded dependencies.

###### parameters

* processEveryFiles (boolean) : default true. Any file encountered that
ends with .properties will be processed.
* includes (list) : default is empty. A list of `include` one entry per file.
The includes will be processed regardless of the value of `processEveryFiles`.
The path must account for relocations and be the exact path relative to the root
 of its original jar. For now no file pattern nor expression is supported,
 and there is no excludes clause : this basically works as a white list.

###### effects

Looks for properties definition matching the format `some.key =
qualified.class.name`. Spaces around the '=' or at the begining/end of
the line will be ignored. For now lists of classes will not be processed
(eg `key = class1, class2, class3`
are invalid). This transformers attempts not to alter the original file
beyond the relocations, comments and indentation should be retrieved
as-is in output file.

**Warning** : Does not merge files in case of duplicate files.


###### credits to

 The original implementation of ```org.apache.maven.plugins.shade.resource.ServicesResourceTransformer```

###### example

```
<plugins>
    <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-shade-plugin</artifactId>

        <configuration>
            <transformers>
                <transformer implementation="org.squashtest.tm.maven.shade.PropertiesRelocatorTransformer">
                    <!-- captures any file that ends with .properties -->
                    <processEveryFiles>true</processEveryFiles>
                    <includes>
                        <!-- this file is structurally a properties file even if not named like one-->
                        <include>relocated/org/mylib/differentsuffix.props</include>
                    </includes>
                </transformer>
            </transformers>

            <relocations>
                <relocation>
                    <pattern>some-pattern</pattern>
                    <shadedPattern>relocated</shadedPattern>
                </relocation>
            </relocations>
        </configuration>

        <dependencies>
            <dependency>
                <groupId>org.squashtest.tm</groupId>
                <artifactId>maven-utils</artifactId>
                <version>1.0.0.RELEASE</version>
            </dependency>
        </dependencies>
    </plugin>
</plugins>
```